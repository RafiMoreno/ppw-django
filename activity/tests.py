from django.test import TestCase, Client
from django.urls import reverse
from .models import *
from .forms import *

class TestCaseMor(TestCase):
    def test_url_activityview(self):
        responses = Client().get('/activity/')
        self.assertEquals(responses.status_code, 200)

    def test_url_activity_template(self):
        responses = Client().get('/activity/')
        self.assertTemplateUsed(responses, 'activity/view.html')

    def test_url_form(self):
        responses = Client().get('/formactivity/')
        self.assertEquals(responses.status_code, 200)
    
    def test_url_form_template(self):
        responses = Client().get('/formactivity/')
        self.assertTemplateUsed(responses, 'activity/formkegiatan.html')

    def test_model(self):
        kegiatan = Activity.objects.create(nama = 'namakegiatan')
        orang = Participant.objects.create(nama = 'namaorang', nama_Kegiatan = kegiatan)
        responses = Client().get('/activity/')
        tmp = responses.content.decode('utf8')
        self.assertIn('namakegiatan', tmp)
        self.assertIn('namaorang', tmp)

    def test_participant_form_valid(self):
        formresponses = self.client.post(reverse('activity:activityform'),data={
            'nama':'mor',
            'nama_Kegiatan':'Tidur'
        })

        self.assertEqual(formresponses.status_code, 200)
    
    def test_url_tambahkegiatan(self):
        responses = Client().get('/formaddactivity/')
        self.assertEquals(responses.status_code, 200)
    
    def test_url_tambahkegiatan_template(self):
        responses = Client().get('/formaddactivity/')
        self.assertTemplateUsed(responses, 'activity/formtambahkegiatan.html')

    def test_addactivity_form_valid(self):
        formresponses = self.client.post(reverse('activity:addactivityform'),data={
            'nama':'Mandi'
        })

        self.assertEqual(formresponses.status_code, 302)
