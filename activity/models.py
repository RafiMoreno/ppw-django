from django.db import models

class Activity(models.Model):
    nama = models.CharField(max_length = 30)
    
    def __str__(self):
        return self.nama

class Participant(models.Model):
    nama = models.CharField(max_length = 50)
    nama_Kegiatan = models.ForeignKey(Activity, null = True, on_delete=models.CASCADE)

    def __str__(self):
        return self.nama
