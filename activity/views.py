from django.shortcuts import render, redirect
from .models import *
from .forms import *
# Create your views here.
def activityview(request):
    activity = Activity.objects.all()
    participant = Participant.objects.all()
    context = { 'activity' : activity,
                'participant' : participant
     }
    return render(request, 'activity/view.html', context)

def activityform(request):
    form = ParticipantForm()

    if request.method == 'POST':
        form = ParticipantForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/activity')

    context = {'form' : form }
    return render(request, 'activity/formkegiatan.html', context)

def addactivityform(request):
    form = ActivityForm()

    if request.method == 'POST':
        form = ActivityForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/formactivity')

    context = {'form' : form }
    return render(request, 'activity/formtambahkegiatan.html', context)

