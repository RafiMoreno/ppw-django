from django import forms
from .models import Activity, Participant

class ParticipantForm(forms.ModelForm):
    class Meta:
        model = Participant
        fields = [
            'nama',
            'nama_Kegiatan'
        ]
        

class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = [
            'nama'
        ]
        

        
        