from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'activity'

urlpatterns = [
    path('activity/', views.activityview, name='activityview'),
    path('formactivity/',views.activityform, name='activityform'),
    path('formaddactivity/',views.addactivityform, name='addactivityform')
]