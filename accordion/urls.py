from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'accordion'

urlpatterns = [
    path('accordion/', views.accordion, name='accordion')
]