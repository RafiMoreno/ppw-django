from django.test import TestCase, Client

class TestCaseAccordion(TestCase):
    def test_url_accordion(self):
        responses = Client().get('/accordion/')
        self.assertEquals(responses.status_code, 200)

    def test_url_accordion_template(self):
        responses = Client().get('/accordion/')
        self.assertTemplateUsed(responses, 'accordion/accordion.html')
