from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

def books(request):
    return render(request, 'books/books.html')

def booksdata(request):
    arg = request.GET['q']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    re = requests.get(url)
    data = json.loads(re.content)
    return JsonResponse(data, safe=False)
