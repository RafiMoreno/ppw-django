from django.test import TestCase, Client

class TestCaseBooks(TestCase):
    def test_url_books(self):
        responses = Client().get('/books/')
        self.assertEquals(responses.status_code, 200)

    def test_url_accordion_template(self):
        responses = Client().get('/books/')
        self.assertTemplateUsed(responses, 'books/books.html')
