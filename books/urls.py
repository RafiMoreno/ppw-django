from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'books'

urlpatterns = [
    path('books/', views.books, name='books'),
    path('booksdata/',views.booksdata, name='booksdata')
]