$(document).ready(function(){
    booksearch('Batman');
    var panel = $('.s7panel').hide();
    $('.s7_accordionhead > p').click(function(){
        $(this).parent().next().slideToggle("fast");
        return false;
    });

    $('.s7_up,.s7_down').click(function(){
        var baris = $(this).parents().parents(".s7_accordionitem:first");
        if ($(this).is(".s7_up")) {
            baris.insertBefore(baris.prev());
        } else if ($(this).is(".s7_down")) {
            baris.insertAfter(baris.next());
    }
    });

    $('#s8_searchbox').keyup(function(){
        var keyword = $('#s8_searchbox').val();
        booksearch(keyword);
    });

});

function booksearch(book){
    $.ajax({
        url : '/booksdata?q=' + book,
        success: function(hasil) {
            console.log(hasil.items);
            var obj_hasil = $("#hasil");
            obj_hasil.empty();  
                for(i=0; i<hasil.items.length; i++){
                    var book_title = hasil.items[i].volumeInfo.title;
                    var book_authors = hasil.items[i].volumeInfo.authors;
                    var book_cover = hasil.items[i].volumeInfo.imageLinks.smallThumbnail;
                    var book_link = hasil.items[i].volumeInfo.canonicalVolumeLink;
                    obj_hasil.append('<tr>'+ '<td>' +'<p>'+ '<a href=' + book_link + '>' + book_title + ' by ' 
                    + book_authors +'<img style="float:left" src =' + book_cover + '>'+ '</a>'+ '</p>' 
                    + '</tr>'+ '</td>');
                }
        }
    })
}