from django.urls import path
from django.conf.urls import url
from . import views


app_name = 'authen'

urlpatterns = [
    path('register/', views.register, name='register'),
    path('login/', views.userlogin, name='login'),
    path('logout/', views.userlogout, name='logout')
]