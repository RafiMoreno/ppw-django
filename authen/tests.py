from django.test import TestCase, Client

class TestCaseAuthen(TestCase):
    def test_url_register(self):
        responses = Client().get("/register/")
        self.assertEquals(responses.status_code, 200)
    
    def test_template_register(self):
        responses = Client().get("/register/")
        self.assertTemplateUsed(responses, 'authen/register.html')
    
    def test_url_login(self):
        responses = Client().get("/login/")
        self.assertEquals(responses.status_code, 200)
    
    def test_template_login(self):
        responses = Client().get("/login/")
        self.assertTemplateUsed(responses, 'authen/login.html')
    
    def test_url_logout(self):
        responses = Client().get("/logout/")
        self.assertEquals(responses.status_code, 302)
