from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from .forms import *
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout

def register(request):
    if request.user.is_authenticated:
        return redirect('/')
    else:
        form = CreateUserForm()
        
        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                username = form.cleaned_data.get('username')
                messages.success(request, 'Account Succesfully Registered')
                return redirect('/login/')
            else:
                messages.warning(request, 'Password does not fulfill requirements')
    
        context = {'form' : form}
        return render(request, 'authen/register.html', context)

def userlogin(request):
    if request.user.is_authenticated:
        return redirect('/')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username , password=password)

            if user is not None:
                login(request, user)
                messages.success(request, 'User ' + username + ' has successfully logged in')
                return redirect('/')
            else:
                messages.warning(request, 'Username / Password does not match ')
        
        context = {}
        return render(request, 'authen/login.html', context)

def userlogout(request):
    logout(request)
    return redirect('/login/')