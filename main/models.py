from django.db import models

class Jadwal(models.Model):
    nama_Matkul = models.CharField(max_length = 75)
    dosen = models.CharField(max_length = 75)
    sks = models.CharField(max_length = 2)
    semester_Tahun = models.CharField(max_length = 50)
    deskripsi_Matkul = models.TextField()
