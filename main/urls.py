from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('jadwal/', views.createJadwal, name='jadwal'),
    path('jadwalview/', views.viewJadwal, name='jadwalview'),
    path('delete/<int:pk>/', views.deleteJadwal, name='delete'),
]
