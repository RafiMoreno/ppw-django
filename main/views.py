from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import JadwalForm
from .models import Jadwal


def home(request):
    return render(request, 'main/home.html')

def about(request):
    return render(request, 'main/about.html')

def story1(request):
    return render(request, 'main/Story1.html')

def story3(request):
    return render(request, 'main/Story3.html')

def story3gallery(request):
    return render(request, 'main/Story3Gallery.html')

def createJadwal(request):
    form = JadwalForm()

    if request.method == 'POST':
        form = JadwalForm(request.POST)
        if form.is_valid():
            form.save()
            form = JadwalForm()

    context = {'form' : form }
    
    return render(request, 'main/Jadwal.html', context)

def viewJadwal(request):
    form = Jadwal.objects.all()
    context = {'form' : form }
    return render(request, 'main/jadwalview.html', context)

def deleteJadwal(request, pk):
    form = Jadwal.objects.get(id=pk)
    if request.method == "POST":
        form.delete()
        return redirect('/jadwalview')
    context = {'form' : form }
    return render(request, 'main/delete.html', context)


