from django import forms
from .models import Jadwal

class JadwalForm(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = [
            'nama_Matkul',
            'dosen',
            'sks',
            'semester_Tahun',
            'deskripsi_Matkul'
        ]
        widgets = {
            'nama_Matkul' : forms.TextInput(attrs={'class' : 'form-control'}),
            'dosen' : forms.TextInput(attrs={'class' : 'form-control'}),
            'sks' : forms.TextInput(attrs={'class' : 'form-control'}),
            'semester_Tahun' : forms.TextInput(attrs={'class' : 'form-control'}),
            'deskripsi_Matkul' : forms.Textarea(attrs={'class' : 'form-control'})
        }